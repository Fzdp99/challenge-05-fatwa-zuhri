link erd challenge 5 : https://dbdiagram.io/d/625cf2712514c979034751e8
link erd tugas sebelumnya : https://dbdiagram.io/d/625ebc101072ae0b6aac0ebe

get / = halaman awal
get /add = halaman add
get /update/:id = halaman update
post /add = menambahkan data
get /delete/:id = menghapus data
post /update = mengedit data
get /filter/:size = filter data
