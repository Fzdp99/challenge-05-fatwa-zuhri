const {
  createdata,
  findAlldata,
  deletedata,
  findIddata,
  updatedata,
  filterdata,
} = require("./db");

// halaman awal
const pageindex = (req, res) => {
  const cars = findAlldata();
  cars.then(function (result) {
    res.render("index", {
      layout: "layouts/main-layout",
      title: "Halaman admin",
      cars: result,
      msg: req.flash("msg"),
    });
  });
};

// halaman add
const pageadd = (req, res) => {
  res.render("add", {
    layout: "layouts/main-layout",
    title: "Halaman tambah data mobil",
  });
};

// halaman update
const pageupdate = (req, res) => {
  const car = findIddata(req.params.id);
  car.then(function (result) {
    res.render("update", {
      layout: "layouts/main-layout",
      title: "Halaman edit data mobil",
      car: result,
    });
  });
};

// menambahkan data
const addDT = (req, res) => {
  createdata(req.body);
  setTimeout(pindah, 2000);

  function pindah() {
    req.flash("msg", "Data mobil berhasil di simpan!");
    res.status(201).redirect("/");
  }
};

// delete data
const deleteDT = (req, res) => {
  deletedata(req.params.id);
  setTimeout(pindah2, 2000);
  function pindah2() {
    res.status(201).redirect("/");
  }
};

// update data
const updateDT = (req, res) => {
  updatedata(req.body);
  setTimeout(pindah3, 2000);
  function pindah3() {
    req.flash("msg", "Data mobil berhasil di edit!");
    res.status(201).redirect("/");
  }
};

// filter data
const filterDT = (req, res) => {
  const cars = filterdata(req.params.size);
  cars.then(function (result) {
    res.render("index", {
      layout: "layouts/main-layout",
      title: "Halaman admin",
      cars: result,
      msg: req.flash("msg"),
    });
  });
};

module.exports = {
  pageindex,
  pageadd,
  pageupdate,
  addDT,
  deleteDT,
  updateDT,
  filterDT,
};
