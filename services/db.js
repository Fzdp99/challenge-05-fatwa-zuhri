const { cars } = require("../models");

// mencari semua data
const findAlldata = async () => {
  const data = await cars.findAll();
  let newdata = JSON.stringify(data, null, 2);
  newdata = JSON.parse(newdata);
  console.log(newdata);
  return newdata;
};

// mencari data berdasarkan id
const findIddata = async (id) => {
  const car = await cars.findOne({
    where: { id: id },
  });
  let newcar = JSON.stringify(car, null, 2);
  newcar = JSON.parse(newcar);
  return newcar;
};

// membuat data baru
const createdata = (data) => {
  cars
    .create({
      name: data.name,
      size: data.size,
      image: data.image,
      price: data.price,
    })
    .then((cars) => {
      console.log(cars);
    });
};

// menghapus data berdasarkan id
const deletedata = (id) => {
  cars.destroy({
    where: {
      id: id,
    },
  });
};

const updatedata = (data) => {
  let newsize = "";
  if (data.size == "Pilih size") {
    newsize = data.oldsize;
  } else {
    newsize = data.size;
  }

  const query = {
    where: { id: data.id },
  };

  cars.update(
    {
      name: data.name,
      size: newsize,
      image: data.image,
      price: data.price,
    },
    query
  );
};

const filterdata = async (size) => {
  const data = await cars.findAll();
  let newdata = JSON.stringify(data, null, 2);
  newdata = JSON.parse(newdata);
  const newcars = newdata.filter((row) => row.size == size);
  return newcars;
};

module.exports = {
  createdata,
  findAlldata,
  deletedata,
  updatedata,
  findIddata,
  filterdata,
};
