--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: dev
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO dev;

--
-- Name: cars; Type: TABLE; Schema: public; Owner: dev
--

CREATE TABLE public.cars (
    id integer NOT NULL,
    name character varying(255),
    size character varying(255),
    image character varying(255),
    price character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.cars OWNER TO dev;

--
-- Name: cars_id_seq; Type: SEQUENCE; Schema: public; Owner: dev
--

CREATE SEQUENCE public.cars_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cars_id_seq OWNER TO dev;

--
-- Name: cars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev
--

ALTER SEQUENCE public.cars_id_seq OWNED BY public.cars.id;


--
-- Name: cars id; Type: DEFAULT; Schema: public; Owner: dev
--

ALTER TABLE ONLY public.cars ALTER COLUMN id SET DEFAULT nextval('public.cars_id_seq'::regclass);


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY public."SequelizeMeta" (name) FROM stdin;
20220419022925-create-cars.js
\.


--
-- Data for Name: cars; Type: TABLE DATA; Schema: public; Owner: dev
--

COPY public.cars (id, name, size, image, price, "createdAt", "updatedAt") FROM stdin;
4	Bmw z4	small	https://cdn.motor1.com/images/mgl/nYqWo/s3/bmw-z4.jpg	20.000.000	2022-04-19 10:44:09.545+07	2022-04-19 10:44:09.545+07
7	Lotus Elise	small	https://www.motoringresearch.com/wp-content/uploads/2020/01/elise-cup-250-9.jpg	50.000.000	2022-04-19 11:19:11.413+07	2022-04-19 11:21:30.09+07
6	Invisible boat mobile	large	https://i.kym-cdn.com/photos/images/original/001/517/172/d4f.jpg	90.000.000	2022-04-19 11:16:27.464+07	2022-04-19 11:36:30.719+07
5	Esemka V2	large	https://cdn-2.tstatic.net/tribunnews/foto/bank/images/esemka-bima11.jpg	2.000	2022-04-19 10:52:12.42+07	2022-04-19 11:47:14.746+07
8	Ferrari Roma	small	http://4.bp.blogspot.com/-IB6f8HOt1Vc/TsUEG5HzZjI/AAAAAAAAAFs/bIwEKqgGJMc/s1600/MOB1.jpg	70.000.000	2022-04-19 11:53:31.342+07	2022-04-19 11:54:02.022+07
9	Cyber truck 	large	https://uploads-ssl.webflow.com/61672da8a96ef1776c5a1374/6172fa5dacc22ba08b4fefac_shutterstock_1634989399.jpg	50.000.000	2022-04-19 12:00:02.949+07	2022-04-19 12:01:02.486+07
10	Dodge challenger	large	https://www.autos.id/wp-content/uploads/2022/01/JDPA_2022-Dodge-Challenger-GT-RWD-with-Hemi-Orange-Appearance-Package-Front-Quarter-View.jpg	20.000.000	2022-04-19 12:04:59.245+07	2022-04-19 12:04:59.245+07
\.


--
-- Name: cars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dev
--

SELECT pg_catalog.setval('public.cars_id_seq', 10, true);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: cars cars_pkey; Type: CONSTRAINT; Schema: public; Owner: dev
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

