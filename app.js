const express = require("express");
const expresslayouts = require("express-ejs-layouts");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const flash = require("connect-flash");
const bp = require("body-parser");
const {
  pageadd,
  pageindex,
  pageupdate,
  addDT,
  deleteDT,
  updateDT,
  filterDT,
} = require("./services/cars");

const app = express();
const port = 3000;

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

// gunakan ejs
app.set("view engine", "ejs");

// template layout
app.use(expresslayouts);

// render file private
app.use(express.static("public"));

// konfigurasi pesan flash
app.use(cookieParser("secret"));
app.use(
  session({
    cookie: { maxAge: 6000 },
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(flash());

// halaman awal
app.get("/", pageindex);

// halaman add
app.get("/add", pageadd);

// halaman update
app.get("/update/:id", pageupdate);

// menambahkan data
app.post("/add", addDT);

// hapus data
app.get("/delete/:id", deleteDT);

// edit data
app.post("/update", updateDT);

// filter data
app.get("/filter/:size", filterDT);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
